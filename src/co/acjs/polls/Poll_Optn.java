package co.acjs.polls;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class Poll_Optn extends Activity {
	int which;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_poll__optn);
		// Show the Up button in the action bar.
		setupActionBar();
		Intent i = getIntent();
		which = i.getIntExtra("which", 0);
		Button b1 = (Button) findViewById(R.id.quest1);
		Button b2 = (Button) findViewById(R.id.quest2);
		Button b3 = (Button) findViewById(R.id.quest3);
		Button b4 = (Button) findViewById(R.id.quest4);
		switch (which) {
		case 1:
			b1.setText(getResources().getString(R.string.qest1_b));
			b2.setText(getResources().getString(R.string.qest2_b));
			b3.setText(getResources().getString(R.string.qest3_b));
			b4.setText(getResources().getString(R.string.qest4_b));
			break;
		case 2:
			b1.setText(getResources().getString(R.string.qest1_c));
			b2.setText(getResources().getString(R.string.qest2_c));
			b3.setText(getResources().getString(R.string.qest3_c));
			b4.setText(getResources().getString(R.string.qest4_c));
			break;
		}
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.poll__optn, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void q1(View v) {
		Intent i = new Intent(this, Question.class);
		i.putExtra("which", which);
		i.putExtra("quest", 1);
		startActivity(i);
	}

	public void q2(View v) {
		Intent i = new Intent(this, Question.class);
		i.putExtra("which", which);
		i.putExtra("quest", 2);
		startActivity(i);
	}

	public void q3(View v) {
		Intent i = new Intent(this, Question.class);
		i.putExtra("which", which);
		i.putExtra("quest", 3);
		startActivity(i);
	}

	public void q4(View v) {
		Intent i = new Intent(this, Question.class);
		i.putExtra("which", which);
		i.putExtra("quest", 4);
		startActivity(i);
	}
}
